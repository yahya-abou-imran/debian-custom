# Debian Custom
Contient le script et les fichiers de configuration que j'utilise pour exécuter
live build.

Live buld est un ensemble de commandes qui permet de générer une image ISO
customisée de Debian GNU/Linux.

Afin de pouvoir utiliser correctement ce script, il est essentiel de consulter
au préalable mon tuto sur le site debian-facile :  
https://debian-facile.org/utilisateurs:abdelqahar:tutos:live-build

### Utilisation 
Il faut d'abord installer `live-build`
```
apt install live-build
```
Ensuite :
```
mkdir dossier_de_travail
cd dossier_de_travil
```
Puis pour chaque projet :
```
git clone https://gitlab.com/yahya-abou-imran/debian-custom.git debian_custom_project
cd debian_custom_project
```
Maintenant, éditez à votre convenance les fichiers décrits dans la partie
`En détails`.
Une fois cela fait :
```
bash auto.sh
```
Et laissez vous guider par le menu.  
J'insiste sur le point qu'il est essentiel de consulter le tuto d'abord !

Le scrit `post-install` est à exécuter en root _après_ l'installation et _avant_
de se logger en user.

### En détails

1. `1000-perso.hook.chroot` :  
Hook qui sera copié dans `config/hooks/`.
2. `includes.etc.skel.txt` :  
Liste des fichiers/rép user qui seront copiés dans
   `config/includes.chroot/etc/skel/`.
3. `includes.root.txt` :  
Liste des fichiers/répertoires système qui seront copiés dans
`config/includes.chroot/`.
4. `live.list.chroot` :  
Liste des paquets supplémentaires présents dans les dépôts qui sera copiée dans
`config/packages-list/`.
5. `packages` :  
Sert à placer des paquets .deb tiers qui seront copiés dans
   `config/pacakges.chroot`.
